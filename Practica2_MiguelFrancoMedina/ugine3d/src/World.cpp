#include "World.h"
#include "Camera.h"

void World::addEntity(const std::shared_ptr<Entity>& entity) {
	listEntities.push_back(entity);

	std::shared_ptr<Camera> camera = std::dynamic_pointer_cast<Camera>(entity);
	if (camera)
		listCameras.push_back(camera);
}

void World::removeEntity(const std::shared_ptr<Entity>& entity) {
	for (auto i = 0; i < listEntities.size(); i++) {
		if (listEntities[i] == entity)
			listEntities.erase(listEntities.begin() + i);
	}

	std::shared_ptr<Camera> camera = std::dynamic_pointer_cast<Camera>(entity);
	if (camera) {
		for (auto i = 0; i < listCameras.size(); i++) {
			if (listCameras[i] == camera)
				listCameras.erase(listCameras.begin() + i);
		}
	}
}

size_t World::getNumEntities() const {
	return listEntities.size();
}

const std::shared_ptr<Entity>& World::getEntity(size_t index) const {
	return listEntities[index];
}

std::shared_ptr<Entity>& World::getEntity(size_t index) {
	return listEntities[index];
}

void World::update(float deltaTime) {
	for (size_t i = 0; i < listEntities.size(); i++)
		listEntities[i]->update(deltaTime);
}

void World::draw() {
	for (size_t i = 0; i < listCameras.size(); i++)
		listCameras[i]->prepare();

	for (size_t i = 0; i < listEntities.size(); i++)
		listEntities[i]->draw();
}
