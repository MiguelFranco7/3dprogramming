#pragma once

#include "common.h"
#include <functional>

class Entity;

typedef std::shared_ptr<Entity>				EntityPtr;
typedef std::shared_ptr<void>				EntityDataPtr;
typedef std::function<void(Entity&, float)> EntityCallback;

class Entity {
public:
	static EntityPtr create();

	const std::string& getName() const;
	void setName(const std::string& name);

	EntityCallback& getCallback();
	void			setCallback(const EntityCallback& func);

	const EntityDataPtr& getUserData() const;
	EntityDataPtr&		 getUserData();
	void				 setUserData(const EntityDataPtr& data);

	const glm::vec3& getPosition() const;
	void			 setPosition(const glm::vec3& pos);

	const glm::quat& getRotationQ() const;				
	const void		 setRotationQ(const glm::quat& rot);

	const glm::vec3& getScale() const;
	void			 setScale(const glm::vec3& scale);

	void			 move(const glm::vec3& vec);

	virtual void	 update(float deltaTime) {}
	virtual void	 draw() {}

	void _update(float deltaTime);

protected:
	Entity();
	virtual ~Entity() {}

	std::string	   mName;
	EntityCallback mCallback;
	EntityDataPtr  mData;
	glm::vec3	   mPosition;
	glm::quat	   mRotationQ;
	glm::vec3	   mScale;
};
