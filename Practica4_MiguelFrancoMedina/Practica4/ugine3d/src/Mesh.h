#pragma once

#include "common.h"
#include "Shader.h"
#include <vector>

class Buffer;
class Material;
class Mesh;
typedef std::shared_ptr<Mesh> MeshPtr;

class Mesh {
public:
	static MeshPtr create();
	void						   addBuffer(const std::shared_ptr<Buffer>& buffer, const std::shared_ptr<Material>& material);
	size_t						   getNumBuffers() const;
	const std::shared_ptr<Buffer>& getBuffer(size_t index) const;
	std::shared_ptr<Buffer>&	   getBuffer(size_t index);
	const std::shared_ptr<Material>& getMaterial(size_t index) const;
	std::shared_ptr<Material>& getMaterial(size_t index);
	void						   draw();

	static MeshPtr load(const char* filename, const ShaderPtr& shader = nullptr);

protected:
	Mesh();

private:
	std::vector<std::pair<std::shared_ptr<Buffer>, std::shared_ptr<Material>>> listBuffers;
};
