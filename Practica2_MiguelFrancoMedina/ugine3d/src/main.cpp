#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include "Ugine3d.h"
#include "../lib/glfw/glfw3.h"
#include <iostream>

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

std::shared_ptr<Shader> State::defaultShader;

int main() {

	// Init glfw
	if ( !glfwInit() ) {
		std::cout << "could not initialize glfw" << std::endl;
		return -1;
	}
	
	// Create window
	//glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_SAMPLES, 8);
	GLFWwindow* win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "", nullptr, nullptr);
	if (!win) {
		std::cout << "could not create opengl window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(win);

	// Init ugine3d
	if (!init()) {
		std::cout << "Could not initialize engine" << std::endl;
		return -1;
	}

	// Define triangle
	std::vector<Vertex> vertices = {
		Vertex(glm::vec3(    0,   0.5f,  0)),
		Vertex(glm::vec3(-0.5f,  -0.5f,  0)),
		Vertex(glm::vec3( 0.5f,  -0.5f,  0))
	};

	// Define index
	std::vector<uint16_t> index = { 0, 1, 2 };

	// Create Shader
	ShaderPtr shaderptr = Shader::create("data/vertex.glsl", "data/fragment.glsl"); 
	if (!shaderptr) {
		std::cout << Shader::getError() << std::endl;
		return -1;
	}
	int mvpLoc = shaderptr->getLocation("mvp");
	shaderptr->use();
	State::defaultShader = shaderptr;

	// Create Buffer
	Bufferptr bufferptr = Buffer::create(vertices, index);

	// Create World
	World world;

	// Create Mesh
	MeshPtr mesh = Mesh::create();
	mesh->addBuffer(bufferptr, nullptr);

	// Create 9 models
	for (float x = -3.0f; x <= 3; x += 3) {
		for (float z = 0.0f; z >= -6; z -= 3) {
			ModelPtr model = Model::create(mesh);
			model->setPosition(glm::vec3(x, 0, z));
			model->setScale(glm::vec3(1, 1, 1));
			world.addEntity(model);
		}
	}

	// Create and add camera
	CameraPtr camera = Camera::create();
	world.addEntity(camera);
	camera->setPosition(glm::vec3(0.0f, 0.0f, 8.0f));

	float rotation = 0.f;

	// View matrix
	glm::mat4 view = glm::lookAt(glm::vec3(0.0f, 0.0f, 6.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	State::viewMatrix = view;

	// Main loop
	double lastTime = glfwGetTime();
	while ( !glfwWindowShouldClose(win) && !glfwGetKey(win, GLFW_KEY_ESCAPE) ) {
		// Get delta time
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Get window size
		int screenWidth, screenHeight;
		glfwGetWindowSize(win, &screenWidth, &screenHeight);

		// Projection matrix
		glm::mat4 projection = glm::perspective<float>(glm::radians(45.0f), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		State::projectionMatrix = projection;
		
		// Increment rotation
		rotation += 60 * deltaTime;

		camera->setProjection(projection);
		camera->setViewport(glm::ivec4(0, 0, screenWidth, screenHeight));
		camera->setClearColor(glm::vec3(0.2, 0.2, 0.2));

		// Print triangles (escala * traslacion * rotacion)
		/*for (float x = -3.0f; x <= 3; x += 3) {
			for (float z = 0.0f; z >= -6; z -= 3) {
				glm::mat4 modelMat = glm::translate(glm::mat4(), glm::vec3(x, 0.0f, z));
				modelMat = glm::rotate(modelMat, glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
				State::modelMatrix = modelMat;
				shaderptr->setMatrix(mvpLoc, projection * view * modelMat);
				bufferptr->draw(*shaderptr);
			}
		}*/

		int nEntities = world.getNumEntities();
		for (int i = 0; i < nEntities; i++) {
			std::shared_ptr<Entity> entity = world.getEntity(i);
			if (entity != camera) {
				entity->setRotationQ(glm::quat(glm::vec3(0, glm::radians(rotation), 0)));
			}
		}

		world.update(deltaTime);
		world.draw();

		// Refresh screen
		glfwSwapBuffers(win);
		glfwPollEvents();
	}

	// Shutdown
	glfwTerminate();
}
