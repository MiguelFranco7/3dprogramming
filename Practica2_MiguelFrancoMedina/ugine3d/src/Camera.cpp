#include "Camera.h"
#include "State.h"

glm::mat4 State::projectionMatrix;
glm::mat4 State::viewMatrix;

CameraPtr Camera::create() {
	std::shared_ptr<Camera> s(new Camera(), [](Camera* p) { delete p; });
	return s;
}

Camera::Camera() : projection(glm::mat4()), viewport(glm::ivec4()), clearColor(glm::vec3()) {}

const glm::mat4 & Camera::getProjection() const {
	return projection;
}

void Camera::setProjection(const glm::mat4 & proj) {
	projection = proj;
}

const glm::ivec4 & Camera::getViewport() const {
	return viewport;
}

void Camera::setViewport(const glm::ivec4 & vp) {
	viewport = vp;
}

const glm::vec3 & Camera::getClearColor() const {
	return clearColor;
}

void Camera::setClearColor(const glm::vec3 & color) {
	clearColor = color;
}

void Camera::prepare() {
	State::projectionMatrix = projection;
	glm::mat4 mat;
	mat = glm::rotate(mat, -glm::angle(rotationQ), glm::axis(rotationQ));
	mat = glm::translate(mat, -position);
	State::viewMatrix = mat;
	
	glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
	glScissor(viewport.x, viewport.y, viewport.z, viewport.w);

	glClearColor(clearColor.x, clearColor.y, clearColor.z, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
