#pragma once

#include "common.h"

class Shader;
typedef std::shared_ptr<Shader> ShaderPtr;

class Shader {
public:
	Shader() {}

	static ShaderPtr create(const std::string& vertexCode, const std::string& fragmentCode);

	uint32_t getId() const;

	static const char* getError();

	void use() const;

	// Activa la escritura de las variables attribute, y especifica su formato
	void setupAttribs() const;

	// Obtiene la localización de una variable uniform
	int getLocation(const char* name) const;

	// Da valor a una variable uniform (deben comprobar que la localización en que se va a escribir no sea -1 antes de hacerlo)
	static void setInt   (int loc, int val);
	static void setFloat (int loc, float val);
	static void setVec2  (int loc, const glm::vec2& vec);
	static void setVec3  (int loc, const glm::vec3& vec);
	static void setVec4  (int loc, const glm::vec4& vec);
	static void setMatrix(int loc, const glm::mat4& matrix);
	
protected:
	Shader(const std::string& vertexCode, const std::string& fragmentCode);
	~Shader();

private:
	uint32_t		   mVertexShader;
	uint32_t		   mFragmentShader;
	uint32_t		   mProgram;
	static std::string mMsgError;
	int				   mVposLoc;
	int				   mVcolorLoc;
	int				   mVtexLoc;
	float			   mVnormalLoc;
};
