#include "Light.h"

LightPtr Light::create() {
	std::shared_ptr<Light> s(new Light(), [](Light* p) { delete p; });
	return s;
}

Light::Type Light::getType() const {
	return mType;
}

void Light::setType(Type type) {
	mType = type;
}

const glm::vec3 & Light::getColor() const {
	return mColor;
}

void Light::setColor(const glm::vec3 & color) {
	mColor = color;
}

float Light::getLinearAttenuation() const {
	return mLinearAttenuation;
}

void Light::setLinearAttenuation(float att) {
	mLinearAttenuation = att;
}

void Light::prepare(int index, ShaderPtr & shader) const { // TODO
	//shader->
}

Light::Light() {

}

Light::~Light() {

}
