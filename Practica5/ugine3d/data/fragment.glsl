uniform sampler2D texSampler;
uniform int		  bTexture;
varying vec2	  ftex;
varying vec3	  fcolor;
varying vec3	  fnormal;
varying vec4	  fpos;

varying vec3  diffuse;
varying vec3  specular;
uniform int   nLights;
uniform int   shininess;
uniform vec3  ambient;
uniform vec3  lightColor[8];
uniform float lightAttenuation[8];
uniform vec4  lightVector[8];
uniform vec4  diffuseColor[8];

void main() {
	specular = vec3(0, 0, 0);
	diffuse   = vec3(0, 0, 0);
	
	if (nLights > 0)
		diffuse = ambient;

	vec3 N = normalize(fnormal);
	for (int i = 0; i < nLights; i++) {
		vec3 L = lightVector[i].xyz;
		float attenuation = 1;
		if (lightVector[i].w == 1) {
			L -= fpos.xyz;
			// TODO: Comprobar si esto va dentro del if
			attenuation = 1.0f / (1.0f + lightAttenuation[i] * length(L)); // TODO: usar constantAttenuation and quadraticAttenuation.
		}

		L = normalize(L);
		float NdotL = dot(N, L);
		if(NdotL < 0.0f) {
			NdotL = 0.0f; // TODO: Se puede usar max
		}

		diffuse += NdotL * lightColor[i] * attenuation;

		if (NdotL > 0.0 && shininess > 0) {
			vec3 H = normalize(L - normalize(fpos).xyz);
			float NdotH = dot(N, H);
			if(NdotH < 0.0) {
				NdotH = 0.0;
			}
			specular += pow(NdotH, shininess) * lightAttenuation[i];
		}
	}

	if (nLights > 0) {
		if (bTexture) {
			gl_FragColor = texture2D(texSampler, ftex) * diffuseColor * vec4(diffuse, 1.0) * vec4(ambient, 1.0) + vec4(specular, 0.0);
		} else {
			gl_FragColor = vec4(fcolor, 1.0) * diffuseColor * vec4(diffuse, 1.0) * vec4(ambient, 1.0) + vec4(specular, 0.0);
		}
	} else {
		if (bTexture) {
			gl_FragColor = texture2D(texSampler, ftex) * diffuseColor; // TODO: diffuseColor igual que vColor?
		} else {
			gl_FragColor = vec4(fcolor, 1.0) * diffuseColor;
		}
	}
}
