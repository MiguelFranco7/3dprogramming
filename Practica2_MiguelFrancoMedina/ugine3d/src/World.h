#pragma once

#include "common.h"
#include <vector>

class Entity;
class Camera;

class World {
public:
	void						   addEntity(const std::shared_ptr<Entity>& entity);
	void						   removeEntity(const std::shared_ptr<Entity>& entity);
	size_t						   getNumEntities() const;
	const std::shared_ptr<Entity>& getEntity(size_t index) const;
	std::shared_ptr<Entity>&	   getEntity(size_t index);
	void						   update(float deltaTime);
	void						   draw();

private:
	std::vector<std::shared_ptr<Entity>> listEntities;
	std::vector<std::shared_ptr<Camera>> listCameras;
};
