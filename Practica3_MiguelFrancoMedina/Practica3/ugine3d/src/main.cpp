#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include "Ugine3d.h"
#include "../lib/glfw/glfw3.h"
#include <iostream>

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

struct TriangleCubeData;
typedef std::shared_ptr<TriangleCubeData> TriangleCubeDataPtr;

struct TriangleCubeData {
	float angle;
	float rotationSpeed;

	TriangleCubeData(float initialAngle, float rotationSpeed) :
		angle(initialAngle),
		rotationSpeed(rotationSpeed) {}

	static void update(Entity& entity, float deltaTime) {
		TriangleCubeDataPtr data = std::static_pointer_cast<TriangleCubeData>(entity.getUserData());
		data->angle += data->rotationSpeed * deltaTime;
		entity.setRotationQ(glm::quat(glm::vec3(0, glm::radians(data->angle), 0)));
	}
};

int main() {

	// Init glfw
	if ( !glfwInit() ) {
		std::cout << "could not initialize glfw" << std::endl;
		return -1;
	}
	
	// Create window
	//glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_SAMPLES, 8);
	GLFWwindow* win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "", nullptr, nullptr);
	if (!win) {
		std::cout << "could not create opengl window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(win);

	// Init ugine3d
	if (!init()) {
		std::cout << "Could not initialize engine" << std::endl;
		return -1;
	}

	// Define coords
	glm::vec3 v0 = glm::vec3(-0.5,  0.5,  0.5);
	glm::vec3 v1 = glm::vec3(-0.5, -0.5,  0.5);
	glm::vec3 v2 = glm::vec3( 0.5, -0.5,  0.5);
	glm::vec3 v3 = glm::vec3( 0.5,  0.5,  0.5);
	glm::vec3 v4 = glm::vec3(-0.5,  0.5, -0.5);
	glm::vec3 v5 = glm::vec3(-0.5, -0.5, -0.5);
	glm::vec3 v6 = glm::vec3( 0.5, -0.5, -0.5);
	glm::vec3 v7 = glm::vec3( 0.5,  0.5, -0.5);

	// Define cube
	std::vector<Vertex> vertices = {
		Vertex(v0, glm::vec2(0, 1)),
		Vertex(v1, glm::vec2(0, 0)),
		Vertex(v2, glm::vec2(1, 0)),

		Vertex(v0, glm::vec2(0, 1)),
		Vertex(v3, glm::vec2(1, 1)),
		Vertex(v2, glm::vec2(1, 0)),

		Vertex(v3, glm::vec2(0, 1)),
		Vertex(v2, glm::vec2(0, 0)),
		Vertex(v6, glm::vec2(1, 0)),

		Vertex(v3, glm::vec2(0, 1)), 
		Vertex(v7, glm::vec2(1, 1)),
		Vertex(v6, glm::vec2(1, 0)),

		Vertex(v4, glm::vec2(1, 1)),
		Vertex(v5, glm::vec2(1, 0)),
		Vertex(v6, glm::vec2(0, 0)),

		Vertex(v4, glm::vec2(1, 1)),
		Vertex(v7, glm::vec2(0, 1)),
		Vertex(v6, glm::vec2(0, 0)),

		Vertex(v0, glm::vec2(1, 1)),
		Vertex(v1, glm::vec2(1, 0)),
		Vertex(v5, glm::vec2(0, 0)),

		Vertex(v0, glm::vec2(1, 1)),
		Vertex(v4, glm::vec2(0, 1)),
		Vertex(v5, glm::vec2(0, 0)),

		// Top
		Vertex(v0, glm::vec2(0, 0)),
		Vertex(v3, glm::vec2(1, 0)),
		Vertex(v7, glm::vec2(1, 1)),

		Vertex(v0, glm::vec2(0, 0)),
		Vertex(v4, glm::vec2(0, 1)),
		Vertex(v7, glm::vec2(1, 1)),

		Vertex(v1, glm::vec2(1, 0)),
		Vertex(v2, glm::vec2(1, 1)),
		Vertex(v5, glm::vec2(0, 0)),

		Vertex(v1, glm::vec2(1, 0)),
		Vertex(v5, glm::vec2(0, 0)),
		Vertex(v6, glm::vec2(1, 0))
	};

	std::vector<uint16_t> indexTop = {
		24, 25, 26,
		27, 28, 29,
		30, 31, 32,
		33, 34, 35
	};

	std::vector<uint16_t> indexFront = {
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23
	};

	// Create World
	WorldPtr world = World::create();

	// Create Shader
	// Load shaders code
	std::string vertexShaderSource   = readString("data/vertex.glsl");
	std::string fragmentShaderSource = readString("data/fragment.glsl");

	ShaderPtr shaderptr = Shader::create(vertexShaderSource, fragmentShaderSource);
	if (!shaderptr) {
		std::cout << Shader::getError() << std::endl;
		return -1;
	}
	int mvpLoc = shaderptr->getLocation("mvp");
	shaderptr->use();
	State::defaultShader = shaderptr;

	// Create Texture
	TexturePtr textureTop   = Texture::load("data/top.png");
	TexturePtr textureFront = Texture::load("data/front.png");

	if (!textureFront || !textureTop) {
		std::cout << "Can't load textures." << std::endl;
		return -1;
	}

	// Create Material
	MaterialPtr materialTop   = Material::create(textureTop, shaderptr);
	MaterialPtr materialFront = Material::create(textureFront, shaderptr);

	// Create Buffer
	Bufferptr bufferTop   = Buffer::create(vertices, indexTop);
	Bufferptr bufferFront = Buffer::create(vertices, indexFront);

	// Create Mesh
	MeshPtr mesh = Mesh::create();
	mesh->addBuffer(bufferTop, materialTop);
	mesh->addBuffer(bufferFront, materialFront);

	// Create model
	ModelPtr model = Model::create(mesh);
	model->setPosition(glm::vec3(0, 0, 0));
	model->setCallback(TriangleCubeData::update);
	model->setUserData(std::make_shared<TriangleCubeData>(0.f, 62.f));
	world->addEntity(model);

	// Create and add camera
	CameraPtr camera = Camera::create(glm::ivec2(SCREEN_WIDTH, SCREEN_HEIGHT));
	world->addEntity(camera);
	camera->setPosition(glm::vec3(0.0f, 1.0f, 3.0f));
	camera->setRotationQ(glm::quat(glm::vec3(glm::radians(-20.f), 0, 0)));

	// Main loop
	double lastTime = glfwGetTime();
	while ( !glfwWindowShouldClose(win) && !glfwGetKey(win, GLFW_KEY_ESCAPE) ) {
		// Get delta time
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Get window size
		int screenWidth, screenHeight;
		glfwGetWindowSize(win, &screenWidth, &screenHeight);

		// Projection matrix
		glm::mat4 projection = glm::perspective<float>(glm::radians(45.0f), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		State::projectionMatrix = projection;

		camera->setProjection(projection);
		camera->setViewport(glm::ivec4(0, 0, screenWidth, screenHeight));

		world->update(deltaTime);
		world->draw();

		// Refresh screen
		glfwSwapBuffers(win);
		glfwPollEvents();
	}

	// Shutdown
	glfwTerminate();
}
