#include "Camera.h"
#include "State.h"

CameraPtr Camera::create(const glm::ivec2& viewportSize) {
	std::shared_ptr<Camera> s(new Camera(viewportSize), [](Camera* p) { delete p; });
	return s;
}

Camera::Camera(const glm::ivec2& viewportSize) {
	setProjection(glm::perspective<float>(glm::radians(60.0f), static_cast<float>(viewportSize.x) / viewportSize.y, 1, 1000));
	setViewport(glm::ivec4(0, 0, viewportSize.x, viewportSize.y));
	setClearColor(glm::vec3(1.f, 1.f, 1.f));
}

const glm::mat4 & Camera::getProjection() const {
	return mProjection;
}

void Camera::setProjection(const glm::mat4 & proj) {
	mProjection = proj;
}

const glm::ivec4 & Camera::getViewport() const {
	return mViewport;
}

void Camera::setViewport(const glm::ivec4 & vp) {
	mViewport = vp;
}

const glm::vec3 & Camera::getClearColor() const {
	return mClearColor;
}

void Camera::setClearColor(const glm::vec3 & color) {
	mClearColor = color;
}

void Camera::prepare() {
	State::projectionMatrix = mProjection;

	glm::mat4 view = glm::mat4(1);
	view = glm::rotate(view, -glm::angle(mRotationQ), glm::axis(mRotationQ));
	view = glm::translate(view, -mPosition);
	State::viewMatrix = view;
	
	glViewport(mViewport.x, mViewport.y, mViewport.z, mViewport.w);
	glScissor(mViewport.x, mViewport.y, mViewport.z, mViewport.w);

	glClearColor(mClearColor.x, mClearColor.y, mClearColor.z, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
