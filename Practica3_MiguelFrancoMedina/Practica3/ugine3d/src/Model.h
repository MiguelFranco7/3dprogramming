#pragma once

#include "Entity.h"
#include "Mesh.h"

class Model;
typedef std::shared_ptr<Model> ModelPtr;

class Model : public Entity {
public:
	static ModelPtr create(const MeshPtr& mesh);

	virtual void draw() override;

protected:
	Model(const MeshPtr& mesh);

private:
	MeshPtr mMesh;
};
