#pragma once

#include "Entity.h"
class Mesh;
class Model;
typedef std::shared_ptr<Model> ModelPtr;

class Model : public Entity {
public:
	static ModelPtr create(const std::shared_ptr<Mesh>& mesh);
	
	virtual void draw() override;

protected:
	Model(const std::shared_ptr<Mesh>& mesh);

private:
	std::shared_ptr<Mesh> mesh;
};
