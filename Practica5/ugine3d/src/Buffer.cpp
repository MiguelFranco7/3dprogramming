#include "common.h"
#include "Buffer.h"
#include "Vertex.h"
#include "Shader.h"

Bufferptr Buffer::create(const std::vector<Vertex>& vertices, const std::vector<uint16_t>& index) {
	std::shared_ptr<Buffer> s(new Buffer(vertices, index), [](Buffer* p) { delete p; });
	return s;
}

Buffer::Buffer(const std::vector<Vertex> &vertices, const std::vector<uint16_t> &index) {
	mVertices = vertices;
	mIndex    = index;
	
	glGenBuffers(1, &mVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * mVertices.size(), mVertices.data(), GL_STATIC_DRAW);
	
	glGenBuffers(1, &mIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * mIndex.size(), mIndex.data(), GL_STATIC_DRAW);
}

Buffer::~Buffer() {
	glDeleteBuffers(1, &mVertexBuffer);
	glDeleteBuffers(1, &mIndexBuffer);
}

void Buffer::draw(const Shader & shader) const {
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);

	shader.setupAttribs();

	glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mIndex.size()), GL_UNSIGNED_SHORT, nullptr);
}
