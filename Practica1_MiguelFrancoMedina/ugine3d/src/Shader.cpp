#include <iostream>
#include "Shader.h"
#include "Utils.h"
#include "Vertex.h"

std::string Shader::msgError;

Shader::Shader(const std::string & vertexCode, const std::string & fragmentCode) {
	// Load shaders code
	std::string vertexShaderSource   = readString("data/vertex.glsl");
	std::string fragmentShaderSource = readString("data/fragment.glsl");

	// Create vertex shader
	int retCode;
	const char* cVertexShaderSource = vertexShaderSource.c_str();
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &cVertexShaderSource, nullptr);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &retCode);
	if (retCode == GL_FALSE) {
		char errorLog[1024];
		glGetShaderInfoLog(vertexShader, sizeof(errorLog), nullptr, errorLog);
		std::cout
			<< "vertex shader could not be compiled:"
			<< std::endl
			<< errorLog
			<< std::endl;
		msgError += "vertex shader could not be compiled: ";
		msgError += errorLog;
		glDeleteShader(vertexShader);
	}

	// Create fragment shader
	const char* cFragmentShaderSource = fragmentShaderSource.c_str();
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &cFragmentShaderSource, nullptr);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &retCode);
	if (retCode == GL_FALSE) {
		char errorLog[1024];
		glGetShaderInfoLog(fragmentShader, sizeof(errorLog), nullptr, errorLog);
		std::cout
			<< "fragment shader could not be compiled:"
			<< std::endl
			<< errorLog
			<< std::endl;

		msgError += "fragment shader could not be compiled: ";
		msgError += errorLog;
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	// Create and link program
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glGetProgramiv(program, GL_LINK_STATUS, &retCode);
	if (retCode == GL_FALSE) {
		char errorLog[1024];
		glGetProgramInfoLog(program, sizeof(errorLog), nullptr, errorLog);
		std::cout
			<< "program could not be linked:"
			<< std::endl
			<< errorLog
			<< std::endl;

		msgError += "program could not be linked: ";
		msgError += errorLog;
		glDeleteProgram(program);
	}

	// Obtener y almacenar la localizacion de todas las variables attribute del shader.
	vposLoc = glGetAttribLocation(program, "vpos");
}

uint32_t Shader::getId() const {
	return program;
}

// Obtiene el mensaje de error.
const char * Shader::getError() {
	return msgError.c_str();
}

void Shader::use() const {
	glUseProgram(program);
}

void Shader::setupAttribs() const {
	if (vposLoc != -1) {
		glEnableVertexAttribArray(vposLoc);
		glVertexAttribPointer(vposLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, mPos)));
	}
}

int Shader::getLocation(const char * name) const {
	return glGetUniformLocation(program, name);
}

void Shader::setInt(int loc, int val) {
	if (loc != -1)
		glUniform1i(loc, val);
}

void Shader::setFloat(int loc, float val) {
	if (loc != -1)
		glUniform1f(loc, val);
}

void Shader::setVec3(int loc, const glm::vec3 & vec) {
	if (loc != -1)
		glUniform3f(loc, vec.x, vec.y, vec.z);
}

void Shader::setVec4(int loc, const glm::vec4 & vec) {
	if (loc != -1)
		glUniform4f(loc, vec.x, vec.y, vec.z, vec.w);
}

void Shader::setMatrix(int loc, const glm::mat4 & matrix) {
	if (loc != -1)
		glUniformMatrix4fv(loc, 1, false, glm::value_ptr(matrix));
}
