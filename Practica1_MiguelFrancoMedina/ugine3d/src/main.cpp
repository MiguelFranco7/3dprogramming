#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include <iostream>
#include "common.h"
#include "Utils.h"
#include "Shader.h"
#include "Buffer.h"
#include "Vertex.h"
#include "../lib/glfw/glfw3.h"

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

bool init() {
	// Initialize opengl extensions
	if (glewInit() != GLEW_OK) {
		std::cout << "could not initialize opengl extensions" << std::endl;
		glfwTerminate();
		return false;
	}

	// Initialize opengl states
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);

	return true;
}

int main() {

	// Init glfw
	if ( !glfwInit() ) {
		std::cout << "could not initialize glfw" << std::endl;
		return -1;
	}
	
	// Create window
	//glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_SAMPLES, 8);
	GLFWwindow* win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "", nullptr, nullptr);
	if (!win) {
		std::cout << "could not create opengl window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(win);

	if (!init()) {
		return -1;
	}

	// Define triangle
	std::vector<Vertex> vertices = {
		Vertex(glm::vec3(0, 0.5,  0)),
		Vertex(glm::vec3(-0.5,  -0.5,  0)),
		Vertex(glm::vec3(0.5,  -0.5,  0))
	};

	// Define index
	std::vector<uint16_t> index = { 0, 1, 2 };

	// Create Shader
	Shader::ShaderPtr shaderptr = Shader::create("data/vertex.glsl", "data/fragment.glsl"); 

	if (!shaderptr) {
		std::cout << Shader::getError() << std::endl;
		return -1;
	}

	shaderptr->use();

	// Create Buffer
	Buffer buffer(vertices, index);

	float rotation = 0.f;

	// Main loop
	double lastTime = glfwGetTime();
	while ( !glfwWindowShouldClose(win) && !glfwGetKey(win, GLFW_KEY_ESCAPE) ) {
		// Get delta time
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Get window size
		int screenWidth, screenHeight;
		glfwGetWindowSize(win, &screenWidth, &screenHeight);

		// Setup viewport
		glViewport(0, 0, screenWidth, screenHeight);
		glScissor (0, 0, screenWidth, screenHeight);

		// Clear screen
		glClearColor(0.2, 0.2, 0.2, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// MVP
		glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		glm::mat4 view		 = glm::lookAt(glm::vec3(0.0f, 0.0f, 6.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		
		// Rotation
		rotation += 60 * deltaTime;

		// Print triangles
		for (float x = -3.0f; x <= 3; x += 3) {
			for (float z = 0.0f; z >= -6; z -= 3) {
				glm::mat4 model = glm::translate(glm::mat4(), glm::vec3(x, 0.0f, z));
				model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
				glm::mat4 mvp = projection * view * model;
				shaderptr->setMatrix(shaderptr->getLocation("mvp"), mvp);
				buffer.draw(*shaderptr);
			}
		}

		// Refresh screen
		glfwSwapBuffers(win);
		glfwPollEvents();
	}

	// Shutdown
	glfwTerminate();
}
