#include "World.h"
#include "State.h"

WorldPtr World::create() {
	std::shared_ptr<World> p(new World, [](World* p) { delete p; });
	return p;
}

World::World() {}

World::~World() {}

void World::addEntity(const EntityPtr& entity) {
	mListEntities.push_back(entity);

	CameraPtr camera = std::dynamic_pointer_cast<Camera>(entity);
	if (camera)
		mListCameras.push_back(camera);
	else {
		LightPtr light = std::dynamic_pointer_cast<Light>(entity);
		if (light)
			mListLights.push_back(light);
	}
}

void World::removeEntity(const EntityPtr& entity) {
	auto it = std::find(mListEntities.begin(), mListEntities.end(), entity);
	if (it != mListEntities.end())
		mListEntities.erase(it);

	CameraPtr cam = std::dynamic_pointer_cast<Camera>(entity);
	if (cam) {
		auto it = std::find(mListCameras.begin(), mListCameras.end(), cam);
		if (it != mListCameras.end())
			mListCameras.erase(it);
	} else {
		LightPtr light = std::dynamic_pointer_cast<Light>(entity);
		if (light) {
			auto it = std::find(mListLights.begin(), mListLights.end(), light);
			if (it != mListLights.end())
				mListLights.erase(it);
		}
	}
}

size_t World::getNumEntities() const {
	return mListEntities.size();
}

const std::shared_ptr<Entity>& World::getEntity(size_t index) const {
	return mListEntities[index];
}

std::shared_ptr<Entity>& World::getEntity(size_t index) {
	return mListEntities[index];
}

const glm::vec3 & World::getAmbient() const {
	return mAmbient;
}

void World::setAmbient(const glm::vec3 & ambient) {
	mAmbient = ambient;
}

void World::update(float deltaTime) {
	for (size_t i = 0; i < mListEntities.size(); i++)
		mListEntities[i]->_update(deltaTime);
}

void World::draw() {
	State::ambient = mAmbient;
	State::lights  = mListLights;

	for (size_t i = 0; i < mListCameras.size(); i++) {
		mListCameras[i]->prepare();

		for (size_t i = 0; i < mListEntities.size(); i++)
			mListEntities[i]->draw();
	}
}
