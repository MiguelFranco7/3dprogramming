#pragma once

#include "common.h"
#include "Camera.h"
#include "Light.h"
#include <vector>

class World;
typedef std::shared_ptr<World> WorldPtr;

class World {
public:
	static WorldPtr create();

	void			 addEntity(const EntityPtr& entity);
	void			 removeEntity(const EntityPtr& entity);
	size_t			 getNumEntities() const;
	const EntityPtr& getEntity(size_t index) const;
	EntityPtr&	     getEntity(size_t index);
	const glm::vec3& getAmbient() const;
	void			 setAmbient(const glm::vec3& ambient);
	void			 update(float deltaTime);
	void			 draw();

protected:
	World();
	~World();

private:
	std::vector<EntityPtr> mListEntities;
	std::vector<CameraPtr> mListCameras;
	std::vector<LightPtr>  mListLights;
	glm::vec3			   mAmbient;
};
