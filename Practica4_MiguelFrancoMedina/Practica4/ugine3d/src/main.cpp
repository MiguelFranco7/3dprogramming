#ifdef _MSC_VER
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include "Ugine3d.h"
#include "../lib/glfw/glfw3.h"
#include <iostream>

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

struct TriangleCubeData;
typedef std::shared_ptr<TriangleCubeData> TriangleCubeDataPtr;

struct TriangleCubeData {
	float angle;
	float rotationSpeed;

	TriangleCubeData(float initialAngle, float rotationSpeed) :
		angle(initialAngle),
		rotationSpeed(rotationSpeed) {}

	static void update(Entity& entity, float deltaTime) {
		TriangleCubeDataPtr data = std::static_pointer_cast<TriangleCubeData>(entity.getUserData());
		data->angle += data->rotationSpeed * deltaTime;
		entity.setRotationQ(glm::quat(glm::vec3(0, glm::radians(data->angle), 0)));
	}
};

int main() {

	// Init glfw
	if ( !glfwInit() ) {
		std::cout << "could not initialize glfw" << std::endl;
		return -1;
	}
	
	// Create window
	//glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_SAMPLES, 8);
	GLFWwindow* win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "", nullptr, nullptr);
	if (!win) {
		std::cout << "could not create opengl window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(win);

	// Init ugine3d
	if (!init()) {
		std::cout << "Could not initialize engine" << std::endl;
		return -1;
	}

	// To implement Camera FP
	glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Create World
	WorldPtr world = World::create();

	// Create Shader
	// Load shaders code
	std::string vertexShaderSource   = readString("data/vertex.glsl");
	std::string fragmentShaderSource = readString("data/fragment.glsl");

	ShaderPtr shaderptr = Shader::create(vertexShaderSource, fragmentShaderSource);
	if (!shaderptr) {
		std::cout << Shader::getError() << std::endl;
		return -1;
	}
	int mvpLoc = shaderptr->getLocation("mvp");
	shaderptr->use();
	State::defaultShader = shaderptr;

	// Create Mesh
	MeshPtr mesh = Mesh::load("data/asian_town.msh.xml", shaderptr);

	// Create model
	ModelPtr model = Model::create(mesh);
	model->setPosition(glm::vec3(0, 0, 0));
	world->addEntity(model);

	// Create and add camera
	glm::vec3 cameraPosition(0.f, 0.02f, 0.f);
	glm::vec3 cameraRotation(0.f, 0.f, 0.f);
	CameraPtr camera = Camera::create(glm::ivec2(SCREEN_WIDTH, SCREEN_HEIGHT));
	world->addEntity(camera);
	camera->setPosition(glm::vec3(cameraPosition));
	camera->setRotationQ(glm::quat(cameraRotation));

	double mouseX, mouseY;
	glfwGetCursorPos(win, &mouseX, &mouseY);
	double speedMX, speedMY, lastMX = mouseX, lastMY = mouseY;

	// Main loop
	double lastTime = glfwGetTime();
	while ( !glfwWindowShouldClose(win) && !glfwGetKey(win, GLFW_KEY_ESCAPE) ) {
		// Get delta time
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Get window size
		int screenWidth, screenHeight;
		glfwGetWindowSize(win, &screenWidth, &screenHeight);

		// Projection matrix
		glm::mat4 projection = glm::perspective<float>(glm::radians(45.0f), (float)screenWidth / (float)screenHeight, 0.01f, 100.0f);
		State::projectionMatrix = projection;

		// Actualizamos coordenadas de raton
		glfwGetCursorPos(win, &mouseX, &mouseY);
		speedMX = static_cast<int>(mouseX - lastMX);
		speedMY = static_cast<int>(mouseY - lastMY);
		lastMX = mouseX;
		lastMY = mouseY;

		// Acualizamos la camara
		if (glfwGetKey(win, GLFW_KEY_W) || glfwGetKey(win, GLFW_KEY_UP))
			camera->move(glm::vec3(0.f, 0.f, -0.0001f));
		if (glfwGetKey(win, GLFW_KEY_S) || glfwGetKey(win, GLFW_KEY_DOWN))
			camera->move(glm::vec3(0.f, 0.f, 0.0001f));
		if (glfwGetKey(win, GLFW_KEY_A) || glfwGetKey(win, GLFW_KEY_LEFT))
			camera->move(glm::vec3(-0.0001f, 0.f, 0.f));
		if (glfwGetKey(win, GLFW_KEY_D) || glfwGetKey(win, GLFW_KEY_RIGHT))
			camera->move(glm::vec3(0.0001f, 0.f, 0.f));

		cameraRotation.y -= speedMX;
		cameraRotation.x -= speedMY;
		camera->setRotationQ(glm::quat(glm::radians(glm::vec3(cameraRotation.x, cameraRotation.y, cameraRotation.z))));
		camera->setProjection(projection);
		camera->setViewport(glm::ivec4(0, 0, screenWidth, screenHeight));

		world->update(deltaTime);
		world->draw();

		// Refresh screen
		glfwSwapBuffers(win);
		glfwPollEvents();
	}

	// Shutdown
	glfwTerminate();
}
