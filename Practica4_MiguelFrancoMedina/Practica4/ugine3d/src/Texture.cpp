#define STB_IMAGE_IMPLEMENTATION

#include "Texture.h"
#include "../stb_image.h"

TexturePtr Texture::create(const uint32_t &id, const glm::ivec2 &size) {
	std::shared_ptr<Texture> s(new Texture(id, size), [](Texture* p) { delete p; });
	return s;
}

TexturePtr Texture::load(const char * filename) {
	stbi_set_flip_vertically_on_load(true); // this is needed as OpenGL expects the first row as the lower one.
	
	glm::ivec2 size;
	unsigned char* image = stbi_load(filename, &size.x, &size.y, nullptr, 4);

	if (!image)
		return nullptr;
	
	GLuint texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	
	return create(texId, size);
}

uint32_t Texture::getId() const {
	return mId;
}

const glm::ivec2 & Texture::getSize() const {
	return mSize;
}

void Texture::bind() const {
	glBindTexture(GL_TEXTURE_2D, mId);
}

Texture::Texture(const uint32_t &id, const glm::ivec2 &size) : mId(id), mSize(size) {}

Texture::~Texture() {
	glDeleteTextures(1, &mId);
}
