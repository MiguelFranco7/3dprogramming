#include "Model.h"
#include "State.h"

Model::Model(const MeshPtr& mesh) : mMesh(mesh) {}

ModelPtr Model::create(const std::shared_ptr<Mesh>& mesh) {
	if (!mesh) return nullptr;
	std::shared_ptr<Model> s(new Model(mesh), [](Model* p) { delete p; });
	return s;
}

void Model::draw() {
	glm::mat4 model = glm::translate(glm::mat4(1), mPosition);
	model = glm::rotate(model, glm::angle(mRotationQ), glm::axis(mRotationQ));
	model = glm::scale(model, mScale);

	State::modelMatrix = model;

	mMesh->draw();
}
