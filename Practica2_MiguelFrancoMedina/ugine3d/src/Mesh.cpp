#include "Mesh.h"
#include "State.h"
#include "Shader.h"
#include "Buffer.h"

glm::mat4 State::modelMatrix;

MeshPtr Mesh::create() {
	std::shared_ptr<Mesh> s(new Mesh(), [](Mesh* p) { delete p; });
	return s;
}

void Mesh::addBuffer(const std::shared_ptr<Buffer>& buffer, const std::shared_ptr<Shader>& shader) {
	if (!shader)
		listBuffers.push_back(std::pair<std::shared_ptr<Buffer>, std::shared_ptr<Shader>>(buffer, State::defaultShader));
	else
		listBuffers.push_back(std::pair<std::shared_ptr<Buffer>, std::shared_ptr<Shader>>(buffer, shader));
}

size_t Mesh::getNumBuffers() const {
	return listBuffers.size();
}

const std::shared_ptr<Buffer>& Mesh::getBuffer(size_t index) const {
	return listBuffers[index].first;
}

std::shared_ptr<Buffer>& Mesh::getBuffer(size_t index) {
	return listBuffers[index].first;
}

void Mesh::draw() {	
	for (size_t i = 0; i < listBuffers.size(); i++) {
		listBuffers[i].second->setupAttribs();
		glm::mat4 mvp = State::projectionMatrix * State::viewMatrix * State::modelMatrix;
		listBuffers[i].second->setMatrix(listBuffers[i].second->getLocation("mvp"), mvp);
		
		listBuffers[i].first->draw(*listBuffers[i].second);
	}
}

Mesh::Mesh() {}
