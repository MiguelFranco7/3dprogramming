#pragma once

#include "Shader.h"
#include "Entity.h"

class Light;
typedef std::shared_ptr<Light> LightPtr;

class Light : public Entity {
public:
	enum Type {
		DIRECTIONAL,
		POINT
	};

	static LightPtr create();

	Type getType() const;
	void setType(Type type);
	
	const glm::vec3& getColor() const;
	void			 setColor(const glm::vec3& color);
	
	float getLinearAttenuation() const;
	void  setLinearAttenuation(float att);
	
	void prepare(int index, ShaderPtr& shader) const;

protected:
	Light();
	~Light();

private:
	Type	  mType;
	glm::vec3 mColor;
	float	  mLinearAttenuation;
};
