#include <iostream>
#include "Mesh.h"
#include "State.h"
#include "Buffer.h"
#include "Material.h"
#include "Utils.h"
#include "Texture.h"
#include "../pugi/pugixml.hpp"

MeshPtr Mesh::create() {
	std::shared_ptr<Mesh> s(new Mesh(), [](Mesh* p) { delete p; });
	return s;
}

void Mesh::addBuffer(const std::shared_ptr<Buffer>& buffer, const std::shared_ptr<Material>& material) {
	listBuffers.push_back(std::make_pair(buffer, material));
}

size_t Mesh::getNumBuffers() const {
	return listBuffers.size();
}

const std::shared_ptr<Buffer>& Mesh::getBuffer(size_t index) const {
	return listBuffers[index].first;
}

std::shared_ptr<Buffer>& Mesh::getBuffer(size_t index) {
	return listBuffers[index].first;
}

const std::shared_ptr<Material> & Mesh::getMaterial(size_t index) const {
	return listBuffers[index].second;
}

std::shared_ptr<Material> & Mesh::getMaterial(size_t index) {
	return listBuffers[index].second;
}

void Mesh::draw() {	
	for (size_t i = 0; i < listBuffers.size(); i++) {		
		listBuffers[i].second->prepare();
		listBuffers[i].first->draw(*listBuffers[i].second->getShader());
	}
}

MeshPtr Mesh::load(const char *filename, const ShaderPtr & shader) {
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("data/asian_town.msh.xml");

	if (result) {
		// Cargado correctamente
		pugi::xml_node meshNode = doc.child("mesh");
		pugi::xml_node buffersNode = meshNode.child("buffers");
		MeshPtr meshPtr = Mesh::create();

		// Iteramos por todos los buffer
		for (pugi::xml_node bufferNode = buffersNode.child("buffer"); bufferNode; bufferNode = bufferNode.next_sibling("buffer")) {
			pugi::xml_node materialNode = bufferNode.child("material");
			std::string textureStr      = materialNode.child("texture").text().as_string();
			std::string indicesStr      = bufferNode.child("indices").text().as_string();
			std::string coordsStr	    = bufferNode.child("coords").text().as_string();
			std::string texcoordsStr    = bufferNode.child("texcoords").text().as_string();

			//std::string texture			 = extractPath(textureStr);
			std::vector<uint16_t> indices	 = splitString<uint16_t>(indicesStr, ',');
			std::vector<float>    coords     = splitString<float>(coordsStr, ',');
			std::vector<float>    texcoords  = splitString<float>(texcoordsStr, ',');

			// Creamos el vector vertex
			std::vector<Vertex> vertices;
			int j = 0;
			for (int i = 0; i < coords.size(); i += 3) {
				vertices.push_back(Vertex(glm::vec3(coords[i], coords[i + 1], coords[i + 2]), glm::vec2(texcoords[j], texcoords[j + 1])));
				j += 2;
			}

			// Creamos el buffer
			Bufferptr buffer = Buffer::create(vertices, indices);

			// Creamos material con textura y shader
			TexturePtr texture = Texture::load(std::string("data/" + textureStr).c_str());
			MaterialPtr material;

			if (!shader)
				material = Material::create(texture, State::defaultShader);
			else
				material = Material::create(texture, shader);

			// A�adimos a la lista de buffers
			meshPtr->listBuffers.push_back(std::make_pair(buffer, material));
		}

		return meshPtr;
		
	} else {
		// No se ha podido cargar
		std::cout << result.description() << std::endl;
		return nullptr;
	}

	return MeshPtr();
}

Mesh::Mesh() {}
