uniform sampler2D texSampler;
uniform int bTexture;
varying vec2 ftex;
varying vec3 fcolor;

void main() {
	if (bTexture) {
		gl_FragColor = texture2D(texSampler, ftex);
	} else {
		gl_FragColor = vec4(fcolor, 1);
	}
}
