#pragma once

struct Vertex {
	glm::vec3 mPos;

	Vertex(glm::vec3 position) {
		mPos = position;
	}
};