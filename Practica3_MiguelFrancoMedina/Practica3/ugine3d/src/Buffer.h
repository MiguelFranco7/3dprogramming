#pragma once

#include "common.h"

class  Shader;
struct Vertex;
class  Buffer;

typedef std::shared_ptr<Buffer> Bufferptr;

class Buffer {

public:
	static Bufferptr create(const std::vector<Vertex> &vertices, const std::vector<uint16_t> &index);
	void draw(const Shader& shader) const;

protected:
	Buffer(const std::vector<Vertex> &vertices, const std::vector<uint16_t> &index);
	~Buffer();

private:
	uint32_t			  mVertexBuffer;
	uint32_t			  mIndexBuffer;
	std::vector<Vertex>   mVertices;
	std::vector<uint16_t> mIndex;
};
