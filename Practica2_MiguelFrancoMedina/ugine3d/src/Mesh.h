#pragma once

#include "common.h"
#include <vector>
class Shader;
class Buffer;
class Mesh;
typedef std::shared_ptr<Mesh> MeshPtr;

class Mesh {
public:
	static MeshPtr create();
	void						   addBuffer(const std::shared_ptr<Buffer>& buffer, const std::shared_ptr<Shader>& shader = nullptr);
	size_t						   getNumBuffers() const;
	const std::shared_ptr<Buffer>& getBuffer(size_t index) const;
	std::shared_ptr<Buffer>&	   getBuffer(size_t index);
	void						   draw();

protected:
	Mesh();

private:
	std::vector<std::pair<std::shared_ptr<Buffer>, std::shared_ptr<Shader>>> listBuffers;
};
