#pragma once

#include <vector>

class Shader;
class Vertex;

class Buffer {

public:
	Buffer(std::vector<Vertex> vertices, std::vector<uint16_t> index);
	~Buffer();
	void draw(const Shader& shader) const;

private:
	uint32_t			  mVertexBuffer;
	uint32_t			  mIndexBuffer;
	std::vector<Vertex>   mVertices;
	std::vector<uint16_t> mIndex;
};
