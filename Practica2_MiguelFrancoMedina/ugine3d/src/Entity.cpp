#include "Entity.h"

Entity::Entity() : position(glm::vec3()), rotationQ(glm::quat()), scale(glm::vec3(1, 1, 1)) {}

const glm::vec3 & Entity::getPosition() const {
	return position;
}

void Entity::setPosition(const glm::vec3 & pos) {
	position = pos;
}

const glm::quat & Entity::getRotationQ() const {
	return rotationQ;
}

const void Entity::setRotationQ(const glm::quat & rot) {
	rotationQ = rot;
}

const glm::vec3 & Entity::getScale() const {
	return scale;
}

void Entity::setScale(const glm::vec3 & _scale) {
	scale = _scale;
}

void Entity::move(const glm::vec3 & vec) {
	position += rotationQ * vec; // traslación += rotación * movimiento
}
