#pragma once

#include "Entity.h"

class Camera;
typedef std::shared_ptr<Camera> CameraPtr;

class Camera : public Entity {
public:
	static CameraPtr create(const glm::ivec2& viewportSize);

	const glm::mat4&  getProjection() const;
	void			  setProjection(const glm::mat4& proj);

	const glm::ivec4& getViewport() const;
	void			  setViewport(const glm::ivec4& vp);

	const glm::vec3&  getClearColor() const;
	void			  setClearColor(const glm::vec3& color);

	void prepare();

protected:
	Camera(const glm::ivec2& viewportSize);

private:
	glm::mat4  mProjection;
	glm::ivec4 mViewport;
	glm::vec3  mClearColor;
};
