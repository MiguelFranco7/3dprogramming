#include "Entity.h"

EntityPtr Entity::create() {
	std::shared_ptr<Entity> p(new Entity, [](Entity* p) { delete p; });
	return p;
}

const std::string & Entity::getName() const {
	return mName;
}

void Entity::setName(const std::string & name) {
	mName = name;
}

EntityCallback & Entity::getCallback() {
	return mCallback;
}

void Entity::setCallback(const EntityCallback & func) {
	mCallback = func;
}

const EntityDataPtr & Entity::getUserData() const {
	return mData;
}

EntityDataPtr & Entity::getUserData() {
	return mData;
}

void Entity::setUserData(const EntityDataPtr & data) {
	mData = data;
}

const glm::vec3 & Entity::getPosition() const {
	return mPosition;
}

void Entity::setPosition(const glm::vec3 & pos) {
	mPosition = pos;
}

const glm::quat & Entity::getRotationQ() const {
	return mRotationQ;
}

const void Entity::setRotationQ(const glm::quat & rot) {
	mRotationQ = rot;
}

const glm::vec3 & Entity::getScale() const {
	return mScale;
}

void Entity::setScale(const glm::vec3 & scale) {
	mScale = scale;
}

void Entity::move(const glm::vec3 & vec) {
	mPosition += mRotationQ * vec;
}

void Entity::_update(float deltaTime) {
	if (mCallback)
		mCallback(*this, deltaTime);

	update(deltaTime);
}

Entity::Entity() : mPosition(glm::vec3()), mRotationQ(glm::quat()), mScale(glm::vec3(1, 1, 1)) {}
