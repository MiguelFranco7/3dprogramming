#pragma once

struct Vertex {
	glm::vec3 mPos;
	glm::vec3 mColor;
	glm::vec2 mUV;
	uint16_t  mNormal; // TODO: Comprobar si es de ese tipo

	Vertex(const glm::vec3 &position, const glm::vec2 &uv) : mPos(position), mColor(glm::vec3(1, 1, 0)), mUV(uv) {}
	Vertex(const glm::vec3 &position, const glm::vec2 &uv, const uint16_t &normal) : mPos(position), mColor(glm::vec3(1, 1, 0)), mUV(uv), mNormal(normal) {}
};
