#include "Shader.h"
#include "Utils.h"
#include "Vertex.h"

std::string Shader::mMsgError;

ShaderPtr Shader::create(const std::string & vertexCode, const std::string & fragmentCode) {
	std::shared_ptr<Shader> s(new Shader(vertexCode, fragmentCode), [](Shader* p) { delete p; });
	if (s->getId() == 0) s = nullptr;
	return s;
}

Shader::Shader(const std::string & vertexCode, const std::string & fragmentCode) : mProgram(0) {
	mMsgError = "";

	// Create vertex shader
	int retCode;
	const char* cVertexShaderSource = vertexCode.c_str();
	mVertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(mVertexShader, 1, &cVertexShaderSource, nullptr);
	glCompileShader(mVertexShader);
	glGetShaderiv(mVertexShader, GL_COMPILE_STATUS, &retCode);
	if (retCode == GL_FALSE) {
		char errorLog[1024];
		glGetShaderInfoLog(mVertexShader, sizeof(errorLog), nullptr, errorLog);
		mMsgError += "vertex shader could not be compiled: ";
		mMsgError += errorLog;
		glDeleteShader(mVertexShader);
		return;
	}

	// Create fragment shader
	const char* cFragmentShaderSource = fragmentCode.c_str();
	mFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(mFragmentShader, 1, &cFragmentShaderSource, nullptr);
	glCompileShader(mFragmentShader);
	glGetShaderiv(mFragmentShader, GL_COMPILE_STATUS, &retCode);
	if (retCode == GL_FALSE) {
		char errorLog[1024];
		glGetShaderInfoLog(mFragmentShader, sizeof(errorLog), nullptr, errorLog);
		mMsgError += "fragment shader could not be compiled: ";
		mMsgError += errorLog;
		glDeleteShader(mVertexShader);
		glDeleteShader(mFragmentShader);
		return;
	}

	// Create and link program
	mProgram = glCreateProgram();
	glAttachShader(mProgram, mVertexShader);
	glAttachShader(mProgram, mFragmentShader);
	glLinkProgram(mProgram);
	glDeleteShader(mVertexShader);
	glDeleteShader(mFragmentShader);
	glGetProgramiv(mProgram, GL_LINK_STATUS, &retCode);
	if (retCode == GL_FALSE) {
		char errorLog[1024];
		glGetProgramInfoLog(mProgram, sizeof(errorLog), nullptr, errorLog);
		mMsgError += "program could not be linked: ";
		mMsgError += errorLog;
		glDeleteProgram(mProgram);
		mProgram = 0;
		return;
	}

	// Obtener y almacenar la localizacion de todas las variables attribute del shader.
	mVposLoc   = glGetAttribLocation(mProgram, "vpos");
	mVcolorLoc = glGetAttribLocation(mProgram, "vcolor");
	mVtexLoc   = glGetAttribLocation(mProgram, "vtex");
}

Shader::~Shader() {
	if (mProgram)
		glDeleteProgram(mProgram);
}

uint32_t Shader::getId() const {
	return mProgram;
}

const char * Shader::getError() {
	return mMsgError.c_str();
}

void Shader::use() const {
	glUseProgram(mProgram);
}

void Shader::setupAttribs() const {
	if (mVposLoc != -1) {
		glEnableVertexAttribArray(mVposLoc);
		glVertexAttribPointer(mVposLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, mPos)));

	}

	if (mVcolorLoc != -1) {
		glEnableVertexAttribArray(mVcolorLoc);
		glVertexAttribPointer(mVcolorLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, mColor)));
	}

	if (mVtexLoc != -1) {
		glEnableVertexAttribArray(mVtexLoc);
		glVertexAttribPointer(mVtexLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, mUV)));
	}
}

int Shader::getLocation(const char * name) const {
	return glGetUniformLocation(mProgram, name);
}

void Shader::setInt(int loc, int val) {
	if (loc != -1)
		glUniform1i(loc, val);
}

void Shader::setFloat(int loc, float val) {
	if (loc != -1)
		glUniform1f(loc, val);
}

void Shader::setVec2(int loc, const glm::vec2 & vec) {
	if (loc != -1)
		glUniform2fv(loc, 1, glm::value_ptr(vec));
}

void Shader::setVec3(int loc, const glm::vec3 & vec) {
	if (loc != -1)
		glUniform3fv(loc, 1, glm::value_ptr(vec));
}

void Shader::setVec4(int loc, const glm::vec4 & vec) {
	if (loc != -1)
		glUniform4fv(loc, 1, glm::value_ptr(vec));
}

void Shader::setMatrix(int loc, const glm::mat4 & matrix) {
	if (loc != -1)
		glUniformMatrix4fv(loc, 1, false, glm::value_ptr(matrix));
}
