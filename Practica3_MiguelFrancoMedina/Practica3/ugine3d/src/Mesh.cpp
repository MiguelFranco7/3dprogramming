#include "Mesh.h"
#include "State.h"
#include "Shader.h"
#include "Buffer.h"
#include "Material.h"

MeshPtr Mesh::create() {
	std::shared_ptr<Mesh> s(new Mesh(), [](Mesh* p) { delete p; });
	return s;
}

void Mesh::addBuffer(const std::shared_ptr<Buffer>& buffer, const std::shared_ptr<Material>& material) {
	listBuffers.push_back(std::make_pair(buffer, material));
}

size_t Mesh::getNumBuffers() const {
	return listBuffers.size();
}

const std::shared_ptr<Buffer>& Mesh::getBuffer(size_t index) const {
	return listBuffers[index].first;
}

std::shared_ptr<Buffer>& Mesh::getBuffer(size_t index) {
	return listBuffers[index].first;
}

const std::shared_ptr<Material> & Mesh::getMaterial(size_t index) const {
	return listBuffers[index].second;
}

std::shared_ptr<Material> & Mesh::getMaterial(size_t index) {
	return listBuffers[index].second;
}

void Mesh::draw() {	
	for (size_t i = 0; i < listBuffers.size(); i++) {
		listBuffers[i].second->prepare();
		listBuffers[i].first->draw(*listBuffers[i].second->getShader());
	}
}

Mesh::Mesh() {}
