#pragma once

#include "common.h"

class Shader {
public:
	typedef std::shared_ptr<Shader> ShaderPtr;

	Shader() {}

	static ShaderPtr create(const std::string& vertexCode, const std::string& fragmentCode) {
		std::shared_ptr<Shader> s(new Shader(vertexCode, fragmentCode), [](Shader* p) { delete p; });
		if (s->getId() == 0) s = nullptr;
		return s;
	}

	// Devuelve el identificador de OpenGL del programa
	uint32_t getId() const;

	// Obtiene el mensaje de error generado al compilar o enlazar
	static const char* getError();

	// Activa el uso de este programa
	void use() const;

	// Activa la escritura de las variables attribute, y especifica su formato
	void setupAttribs() const;

	// Obtiene la localización de una variable uniform
	int getLocation(const char* name) const;

	// Da valor a una variable uniform (deben comprobar que la localización en que se va a escribir no sea -1 antes de hacerlo)
	void setInt   (int loc, int val);
	void setFloat (int loc, float val);
	void setVec3  (int loc, const glm::vec3& vec);
	void setVec4  (int loc, const glm::vec4& vec);
	void setMatrix(int loc, const glm::mat4& matrix);

	
protected:
	Shader(const std::string& vertexCode, const std::string& fragmentCode);
	~Shader() { glDeleteProgram(program); }

private:
	uint32_t		   vertexShader;
	uint32_t		   fragmentShader;
	uint32_t		   program;
	static std::string msgError;

	int vposLoc;
};