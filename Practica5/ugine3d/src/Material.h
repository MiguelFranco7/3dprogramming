#pragma once

#include "common.h"

class Material;
class Shader;
class Texture;

typedef std::shared_ptr<Material> MaterialPtr;
typedef std::shared_ptr<Shader> ShaderPtr;
typedef std::shared_ptr<Texture> TexturePtr;

class Material {
public:
	static MaterialPtr create(const TexturePtr& tex = nullptr, const ShaderPtr& shader = nullptr);
	
	const ShaderPtr& getShader() const;
	ShaderPtr& getShader();
	void setShader(const ShaderPtr& shader);

	const TexturePtr& getTexture() const;
	void setTexture(const TexturePtr& tex);

	const glm::vec4& getColor() const;
	void			 setColor(const glm::vec4& color);

	uint8_t getShininess() const;
	void    setShininess(uint8_t shininess);
	
	void prepare();

protected:
	Material(const TexturePtr& tex = nullptr, const ShaderPtr& shader = nullptr);

private:
	TexturePtr mTexturePtr;
	ShaderPtr  mShaderPtr;
	
	glm::vec4 mColor;
	uint8_t   mShininess;
};
