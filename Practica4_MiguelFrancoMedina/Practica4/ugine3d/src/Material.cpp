﻿#include "Material.h"
#include "State.h"
#include "Shader.h"
#include "Texture.h"

MaterialPtr Material::create(const TexturePtr & tex, const ShaderPtr & shader) {
	std::shared_ptr<Material> s(new Material(tex, shader), [](Material* p) { delete p; });
	return s;
}

const ShaderPtr & Material::getShader() const {
	if (mShaderPtr)
		return mShaderPtr;
	else
		return State::defaultShader;
}

ShaderPtr & Material::getShader() {
	if (mShaderPtr)
		return mShaderPtr;
	else
		return State::defaultShader;
}

void Material::setShader(const ShaderPtr & shader) {
	mShaderPtr = shader;
}

const TexturePtr & Material::getTexture() const {
	return mTexturePtr;
}

void Material::setTexture(const TexturePtr & tex) {
	mTexturePtr = tex;
}

void Material::prepare() {
	// Activamos el shader devuelto por ​getShader
	ShaderPtr shader = getShader();
	shader->setupAttribs();
	shader->use();

	// Escribimos las variables uniformes necesarias. Debemos crear variables uniformes nuevas para indicar si se debe utilizar textura (en caso contrario el objeto se pinta de blanco), y el sampler de la textura
	glm::mat4 mvp = State::projectionMatrix * State::viewMatrix * State::modelMatrix;
	shader->setMatrix(mShaderPtr->getLocation("mvp"), mvp);
	shader->setInt(getShader()->getLocation("texSampler"), 0);

	if (mTexturePtr) {
		shader->setInt(shader->getLocation("bTexture"), 1);
		mTexturePtr->bind();
	}
	else
		shader->setInt(shader->getLocation("bTexture"), 0);
}

Material::Material(const TexturePtr & tex, const ShaderPtr & shader) : mTexturePtr(tex), mShaderPtr(shader) {}
