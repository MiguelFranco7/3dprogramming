﻿#include "Material.h"
#include "State.h"
#include "Shader.h"
#include "Texture.h"

MaterialPtr Material::create(const TexturePtr & tex, const ShaderPtr & shader) {
	std::shared_ptr<Material> s(new Material(tex, shader), [](Material* p) { delete p; });
	return s;
}

const ShaderPtr & Material::getShader() const {
	if (mShaderPtr)
		return mShaderPtr;
	else
		return State::defaultShader;
}

ShaderPtr & Material::getShader() {
	if (mShaderPtr)
		return mShaderPtr;
	else
		return State::defaultShader;
}

void Material::setShader(const ShaderPtr & shader) {
	mShaderPtr = shader;
}

const TexturePtr & Material::getTexture() const {
	return mTexturePtr;
}

void Material::setTexture(const TexturePtr & tex) {
	mTexturePtr = tex;
}

const glm::vec4 & Material::getColor() const {
	return mColor;
}

void Material::setColor(const glm::vec4 & color) {
	mColor = color;
}

uint8_t Material::getShininess() const {
	return mShininess;
}

void Material::setShininess(uint8_t shininess) {
	mShininess = shininess;
}

void Material::prepare() { // TODO: Cambios y pasar al shader los datos
	// Activamos el shader devuelto por ​getShader
	ShaderPtr shader = getShader();
	shader->setupAttribs();
	shader->use();

	glm::mat4 mvp = State::projectionMatrix * State::viewMatrix * State::modelMatrix;
	shader->setMatrix(mShaderPtr->getLocation("mvp"), mvp);
	shader->setInt(getShader()->getLocation("texSampler"), 0);

	if (mTexturePtr) {
		shader->setInt(shader->getLocation("bTexture"), 1);
		mTexturePtr->bind();
	}
	else
		shader->setInt(shader->getLocation("bTexture"), 0);
}

Material::Material(const TexturePtr & tex, const ShaderPtr & shader) : mTexturePtr(tex), mShaderPtr(shader) {}
