#include "Model.h"
#include "State.h"
#include "Mesh.h"
#include <iostream>

Model::Model(const std::shared_ptr<Mesh>& _mesh) : mesh(_mesh) {}

ModelPtr Model::create(const std::shared_ptr<Mesh>& mesh) {
	std::shared_ptr<Model> s(new Model(mesh), [](Model* p) { delete p; });
	return s;
}

void Model::draw() {
	glm::mat4 model;
	model =	glm::translate(model, position);
	model = glm::rotate(model, glm::angle(rotationQ), glm::axis(rotationQ));
	model = glm::scale(model, scale);

	State::modelMatrix = model;

	mesh->draw();
}
