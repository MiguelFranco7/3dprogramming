#pragma once

#include "common.h"

class Entity {
public:
	Entity();
	virtual ~Entity() {}

	const glm::vec3& getPosition() const;
	void			 setPosition(const glm::vec3& pos);

	const glm::quat& getRotationQ() const;				
	const void		 setRotationQ(const glm::quat& rot);

	const glm::vec3& getScale() const;
	void			 setScale(const glm::vec3& scale);

	void			 move(const glm::vec3& vec);

	virtual void	 update(float deltaTime) {}
	virtual void	 draw() {}

protected:
	glm::vec3 position;
	glm::quat rotationQ;
	glm::vec3 scale;
};
