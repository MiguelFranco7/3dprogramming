#pragma once

struct Vertex {
	glm::vec3 mPos;
	glm::vec3 mColor;
	glm::vec2 mUV;

	Vertex(const glm::vec3 &position, const glm::vec2 &uv) : mPos(position), mColor(glm::vec3(1, 1, 0)), mUV(uv) {}
};
