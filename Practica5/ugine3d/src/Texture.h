#pragma once

#include "common.h"

class Texture;
class Mesh;

typedef std::shared_ptr<Texture> TexturePtr;

class Texture {

public:
	static TexturePtr create(const uint32_t &id, const glm::ivec2 &size);
	static TexturePtr load(const char* filename);
	uint32_t		  getId() const;
	const glm::ivec2& getSize() const;
	void			  bind() const;

protected:
	Texture(const uint32_t &id, const glm::ivec2 &size);
	~Texture();

private:
	uint32_t     mId;
	glm::ivec2   mSize;
};
